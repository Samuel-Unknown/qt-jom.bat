# README #

### Назначение репозитория ###

В Qt Creator есть проблема с выводом в окне отладки кириллицы при использовании компилитора msvc, в этом README описан способ устранения этой проблемы.

**Было:**

![2.png](https://bitbucket.org/repo/BLgR4j/images/1481919011-2.png)

**Стало:**

![1.png](https://bitbucket.org/repo/BLgR4j/images/3013985796-1.png)

### Что нужно сделать? ###
* Создать файл jom.bat со следующим содержимым (измените путь если нужно)
```
#!bat

@echo off
chcp 1251 > nul
C:\Qt\Qt5.7.1_msvc2015x64\Tools\QtCreator\bin\jom.exe %*
```
* Добавить в папку где установлена Qt файл jom.bat, положить его можно туда же где лежит jom.exe (C:\Qt\Qt5.7.1_msvc2015x64\Tools\QtCreator\bin)

* для исправления нужно в настройках проекта изменить путь к jom.exe на путь к Jom.bat

![qt.png](https://bitbucket.org/repo/BLgR4j/images/1722991762-qt.png)